// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "test.h"

#include "ast/nodes/scope.h"
#include "parser/parser.h"
#include "lexer/lexer.h"
#include "logger/logger.h"

#include <string>
#include <vector>

namespace Test
{
	class TestEvaluator final : public Test
	{
	public:
		TestEvaluator() = default;
		~TestEvaluator() override = default;

		void Init() override{};

		bool Run() const override
		{
			// AUTOMATICALLY GENERATED FUNCTION, CHANGES WILL BE OVERWRITTEN
			return true;
		};
	};
};
