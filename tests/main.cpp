// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "test_lexer.h"
#include "test_evaluator.h"
#include "test_parser.h"

#include "logger/logger.h"

#include <vector>

int main()
{
	long failed_tests = 0;

	std::vector<Test::Test *> tests;
	tests.push_back(new Test::TestLexer());
	tests.push_back(new Test::TestParser());
	tests.push_back(new Test::TestEvaluator());

	long test_count = static_cast<long>(tests.size());

	for (Test::Test *test : tests)
	{
		test->Init();
		if (!test->Run())
		{
			failed_tests++;
		}
	}

	for (Test::Test *test : tests)
	{
		delete test;
	}

	if (failed_tests)
	{
		LOGGER.Error("%ld/%ld tests failed!\n", failed_tests, test_count);
		return 1;
	}

	LOGGER.Info("All %ld/%ld tests succeeded!\n", test_count, test_count);
	return 0;
}
