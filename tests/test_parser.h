// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "test.h"

#include "ast/nodes/scope.h"
#include "parser/parser.h"
#include "lexer/lexer.h"
#include "logger/logger.h"

#include <string>
#include <vector>

namespace Test
{

	std::string TestLoggerOutput;

	class TestParser final : public Test
	{
	public:
		TestParser() = default;
		~TestParser() override = default;

		void Init() override{};

		static int TestLogger(FILE *, const char *fmt, va_list ap)
		{
			std::string temp;

			va_list ap_copy;
			va_copy(ap_copy, ap);
			size_t len = vsnprintf(nullptr, 0, fmt, ap_copy);
			temp.resize(len + 1); // +1 for null terminator
			vsnprintf(&temp[0], len + 1, fmt, ap);
			temp.resize(len);

			TestLoggerOutput += temp;
			return 0;
		}

		bool Run() const override
		{
			std::vector<Lexer::Token> tokens;

			tokens.emplace_back(Lexer::Token::FUNCTION, "main");
			tokens.emplace_back(Lexer::Token::SCOPE_OPEN, "");
			{
				std::vector<Lexer::Token> expressions;
				expressions.emplace_back(Lexer::Token::TERM, "1");
				expressions.emplace_back(Lexer::Token::OPERATOR, "==");
				expressions.emplace_back(Lexer::Token::TERM, "0");
				tokens.emplace_back(Lexer::Token::CONDITION, "", expressions);
			}
			tokens.emplace_back(Lexer::Token::BLOCK_OPEN, "then");
			{
				std::vector<Lexer::Token> expressions;
				expressions.emplace_back(Lexer::Token::TERM, "false");
				tokens.emplace_back(Lexer::Token::RETURN, "", expressions);
			}
			tokens.emplace_back(Lexer::Token::BLOCK_EXIT, "");
			tokens.emplace_back(Lexer::Token::BLOCK_OPEN, "else");
			tokens.emplace_back(Lexer::Token::BLOCK_EXIT, "");
			{
				std::vector<Lexer::Token> expressions;
				expressions.emplace_back(Lexer::Token::TERM, "10");
				expressions.emplace_back(Lexer::Token::OPERATOR, "+");
				expressions.emplace_back(Lexer::Token::TERM, "50");
				tokens.emplace_back(Lexer::Token::RETURN, "", expressions);
			}
			tokens.emplace_back(Lexer::Token::SCOPE_EXIT, "");

			auto parser = Parser::Parser(tokens);
			auto program = parser.parse();

			std::string expected_program = "> Scope\n"
										   "-> FunctionDeclarationStatement - main\n"
										   "--> Scope\n"
										   "---> ConditionalStatement\n"
										   "----> BinaryExpression - ==\n"
										   "-----> LiteralExpression - 1.000000\n"
										   "-----> LiteralExpression - 0.000000\n"
										   "----> AggregateStatement\n"
										   "-----> ReturnStatement\n"
										   "------> StoredExpression - false\n"
										   "----> AggregateStatement\n"
										   "---> ReturnStatement\n"
										   "----> BinaryExpression - +\n"
										   "-----> LiteralExpression - 10.000000\n"
										   "-----> LiteralExpression - 50.000000\n"
										   "-> ReturnStatement\n"
										   "--> CallExpression - main\n"
										   "---> MapExpression -\n";

			LOGGER.SetPrintFunction(TestLogger);
			program->print(0);
			LOGGER.SetPrintFunction(vfprintf);

			if (TestLoggerOutput != expected_program)
			{
				LOGGER.Error("TestParser failed: Parsed AST does not match with expected AST");
				return false;
			}

			return true;
		};
	};
};
