// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "test.h"
#include "lexer/lexer.h"
#include "logger/logger.h"

#include <string>

namespace Test
{
	class TestLexer final : public Test
	{
	public:
		TestLexer() = default;

		~TestLexer() override = default;

		void Init() override{};

		bool Run() const override
		{
			const std::string test_script =
				"function foo [\n"
				"\tcondition 1 == 0 then {\n"
				"\t\treturn false;\n"
				"\t}\n"
				"\n"
				"\treturn 10 + 50;\n"
				"]\n"
				"\n"
				"function bar [\n"
				"\tcondition 1 == 1 then {\n"
				"\t\treturn 1;\n"
				"\t} else {\n"
				"\t\treturn 0;\n"
				"\t}\n"
				"]\n"
				"\n"
				"function main [\n"
				"\tstore foo_ret = call foo with (param: 1, param2: 2);\n"
				"\n"
				"\tstore test_array = (1, 2, 3, 4);\n"
				"\tstore test_array(0) = 3;\n"
				"\n"
				"\tstore test_map = (a: 1, b: 2, c: 3);\n"
				"\tstore test_map(\"a\") = 4;\n"
				"\n"
				"\tloop foo_ret < 100 {\n"
				"\t\tstore foo_ret = foo_ret + 1;\n"
				"\t}\n"
				"\n"
				"\tstore print_ret = call sys_print with (str: foo_ret);\n"
				"\n"
				"\treturn foo_ret;\n"
				"]";

			auto lexer = Lexer::Lexer(test_script);
			auto tokens = lexer.parse();

			std::string tokens_stringified;
			for (const auto &token : tokens)
			{
				tokens_stringified += token.to_string() + " ";
			}

			const std::string expected_tokens_stringified = "FUNCTION - foo SCOPE_OPEN CONDITION { TERM - 1  OPERATOR - ==  TERM - 0 }  BLOCK_OPEN - then RETURN { TERM - false }  BLOCK_EXIT BLOCK_OPEN - else BLOCK_EXIT SCOPE_EXIT FUNCTION - bar SCOPE_OPEN CONDITION { TERM - 1  OPERATOR - ==  TERM - 1 }  BLOCK_OPEN - then RETURN { TERM - 1 }  BLOCK_EXIT BLOCK_OPEN - else RETURN { TERM - 0 }  BLOCK_EXIT SCOPE_EXIT FUNCTION - main SCOPE_OPEN STORE - foo_ret { TERM - call { MAP_OPEN  MAP_KEY - param { TERM - 1 }   MAP_KEY - param2 { TERM - 2 }   MAP_EXIT }   TERM - foo }  STORE - test_array { LIST_OPEN  TERM - 1  TERM - 2  TERM - 3  TERM - 4  LIST_EXIT }  STORE - test_array(0) { TERM - 3 }  STORE - test_map { MAP_OPEN  MAP_KEY - a { TERM - 1 }   MAP_KEY - b { TERM - 2 }   MAP_KEY - c { TERM - 3 }   MAP_EXIT }  STORE - test_map(\"a\") { TERM - 4 }  LOOP { TERM - foo_ret  OPERATOR - <  TERM - 100 }  BLOCK_OPEN STORE - foo_ret { TERM - foo_ret  OPERATOR - +  TERM - 1 }  BLOCK_EXIT STORE - print_ret { TERM - call { MAP_OPEN  MAP_KEY - str { TERM - foo_ret }   MAP_EXIT }   TERM - sys_print }  RETURN { TERM - foo_ret }  SCOPE_EXIT ";

			if (expected_tokens_stringified == tokens_stringified)
			{
				return true;
			}

			LOGGER.Error("TestLexer failed: Token list does not match with expected list");
			return false;
		};
	};
};
