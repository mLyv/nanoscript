cmake_minimum_required(VERSION 3.17)
project(nanoscript)
set(CMAKE_CXX_STANDARD 20)

add_subdirectory(lib)
add_subdirectory(src)
add_subdirectory(tests)
