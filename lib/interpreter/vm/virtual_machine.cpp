// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "virtual_machine.h"
#include "interpreter/data_handles/value.h"
#include "interpreter/vm/exception/exception.h"
#include "logger/logger.h"

#include <chrono>
#include <thread>

std::map<std::string, void (Interpreter::VM::VirtualMachine::*)(Interpreter::Value *) const> Interpreter::VM::VirtualMachine::NATIVE_FUNCTIONS = {
	{"print", &Interpreter::VM::VirtualMachine::native_print},
	{"sleep", &Interpreter::VM::VirtualMachine::native_sleep},
};

bool Interpreter::VM::VirtualMachine::call_native(std::string name, Value *params) const
{
	auto it = NATIVE_FUNCTIONS.find(name);
	if (it == NATIVE_FUNCTIONS.end())
	{
		return false;
	}

	auto native_func = it->second;
	(this->*native_func)(params);

	return true;
}

void Interpreter::VM::VirtualMachine::native_print(Value *params) const
{
	if (has_error())
	{
		return;
	}

	LOGGER.Raw(stdout, "\n%s", params->get_value("str")->to_string().c_str());
}

void Interpreter::VM::VirtualMachine::native_sleep(Value *params) const
{
	// TODO: Parameter error checking

	if (has_error())
	{
		return;
	}

	std::this_thread::sleep_for(std::chrono::milliseconds(static_cast<long>(params->get_value("duration")->as_number())));
}

void Interpreter::VM::VirtualMachine::raise_exception(const Exception &e)
{
	if (has_error())
	{
		return;
	}

	LOGGER.Error("%s\n", e.to_string().c_str());
	m_has_error = true;
}
