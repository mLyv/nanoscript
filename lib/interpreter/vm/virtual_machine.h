// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <map>
#include <string>

namespace Interpreter
{

	class Value;
	class Func;

	class Exception;

	namespace VM
	{

		class VirtualMachine
		{
		public:
			virtual ~VirtualMachine() = default;

			virtual void pop_stack() = 0;

			virtual void push_stack() = 0;

			virtual void set_value(std::string name, Value *new_value) = 0;

			virtual Value *get_value(std::string name) = 0;

			virtual void set_function(std::string name, Func *new_function) = 0;

			virtual Func *get_function(std::string name) = 0;

			void raise_exception(const Exception &e);

			void reset_error() { m_has_error = false; };

			bool has_error() const { return m_has_error; };

			bool call_native(std::string name, Value *params) const;

			// natives

			static std::map<std::string, void (VirtualMachine::*)(Value *) const> NATIVE_FUNCTIONS;

			void native_print(Value *params) const;
			void native_sleep(Value *params) const;

		protected:
			VirtualMachine() = default;

			bool m_has_error = false;
		};

	}

}
