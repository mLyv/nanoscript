// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <string>
#include <utility>

namespace Interpreter
{

	class Exception
	{
	public:
		virtual ~Exception() = default;
		virtual std::string to_string() const = 0;

	protected:
		Exception() = default;
	};

	class VariableNotFoundException final : public Exception
	{
	public:
		VariableNotFoundException(std::string name) : m_name(std::move(name)){};
		~VariableNotFoundException() override = default;

		std::string to_string() const override;

	private:
		std::string m_name;
	};

	class FunctionNotFoundException final : public Exception
	{
	public:
		FunctionNotFoundException(std::string name) : m_name(std::move(name)){};
		~FunctionNotFoundException() override = default;

		std::string to_string() const override;

	private:
		std::string m_name;
	};

	class InvalidExpressionException final : public Exception
	{
	public:
		InvalidExpressionException() = default;
		~InvalidExpressionException() override = default;

		std::string to_string() const override;
	};

	class InvalidScopeException final : public Exception
	{
	public:
		InvalidScopeException() = default;
		~InvalidScopeException() override = default;

		std::string to_string() const override;
	};

};
