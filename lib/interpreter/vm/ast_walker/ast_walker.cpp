// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "ast_walker.h"

#include "interpreter/data_handles/func.h"
#include "interpreter/data_handles/value.h"
#include "interpreter/vm/exception/exception.h"

namespace Interpreter::VM
{

	std::string ASTWalker::SCOPE_EXIT_VALUE = "SCOPE_EXIT_VALUE";
	std::string ASTWalker::SCOPE_REENTRY_VALUE = "SCOPE_REENTRY_VALUE";

	ASTWalker::~ASTWalker()
	{
		for (const auto &entry : m_functions)
		{
			delete entry.second;
		}
	}

	void ASTWalker::pop_stack()
	{
		if (has_error())
		{
			return;
		}

		if (m_stack.size() == 1)
		{
			return;
		}

		Value *ret_value = m_current_stack->get_data(SCOPE_EXIT_VALUE);
		Value *cloned_ret_value = ret_value->make_copy();

		m_stack.pop_back();
		m_current_stack = &m_stack.back();
		m_current_stack->set_data(SCOPE_REENTRY_VALUE, cloned_ret_value);
	}

	void ASTWalker::push_stack()
	{
		if (has_error())
		{
			return;
		}

		m_stack.emplace_back();
		m_current_stack = &m_stack.back();

		m_current_stack->set_data(SCOPE_REENTRY_VALUE, new Value()); // Set default to prevent segfaults
		m_current_stack->set_data(SCOPE_EXIT_VALUE, new Value());	 // Set default to prevent segfaults
	}

	void ASTWalker::set_value(std::string name, Value *new_value)
	{
		if (has_error())
		{
			return;
		}

		try
		{
			m_current_stack->set_data(name, new_value);
		}
		catch (const std::out_of_range &e)
		{
			raise_exception(VariableNotFoundException(name));
		}
	}

	Value *ASTWalker::get_value(std::string name)
	{
		if (has_error())
		{
			return nullptr;
		}

		Value *value = nullptr;

		try
		{
			value = m_current_stack->get_data(name);
		}
		catch (const std::out_of_range &e)
		{
			raise_exception(VariableNotFoundException(name));
		}

		return value;
	}

	void ASTWalker::set_function(std::string name, Func *new_function)
	{
		if (has_error())
		{
			return;
		}

		try
		{
			if (m_functions.contains(name))
			{
				delete m_functions.at(name);
				m_functions[name] = new_function;

				return;
			}

			m_functions.insert(std::make_pair(name, new_function));
		}
		catch (const std::out_of_range &e)
		{
			raise_exception(FunctionNotFoundException(name));
		}
	}

	Func *ASTWalker::get_function(std::string name)
	{
		if (has_error())
		{
			return nullptr;
		}

		Func *func = nullptr;

		try
		{
			func = m_functions.at(name);
		}
		catch (const std::out_of_range &e)
		{
			raise_exception(FunctionNotFoundException(name));
		}

		return func;
	}

};
