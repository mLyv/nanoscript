// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <list>
#include <map>

#include "interpreter/stack/stackframe.h"
#include "interpreter/vm/virtual_machine.h"

namespace Interpreter
{

	class Value;

	class Func;

	namespace VM
	{

		class ASTWalker final : public VirtualMachine
		{
		public:
			ASTWalker() : m_current_stack(nullptr){};

			ASTWalker(const ASTWalker &) = delete;
			ASTWalker(const ASTWalker &&) = delete;
			ASTWalker &operator=(const ASTWalker &) = delete;
			ASTWalker &operator=(ASTWalker &&) = delete;

			~ASTWalker() override;

			static std::string SCOPE_EXIT_VALUE;
			static std::string SCOPE_REENTRY_VALUE;

			void pop_stack() override; // SCOPE_EXIT_VALUE returned
			void push_stack() override;

			void set_value(std::string name, Value *new_value) override;

			Value *get_value(std::string name) override;

			void set_function(std::string name, Func *new_function) override;

			Func *get_function(std::string name) override;

		private:
			StackFrame *m_current_stack;
			std::list<StackFrame> m_stack;

			std::map<std::string, Func *> m_functions;
		};

	};

};
