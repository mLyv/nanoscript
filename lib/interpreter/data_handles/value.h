// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <cstdint>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

namespace Interpreter
{

	namespace VM
	{
		class ASTWalker;
	};

	typedef VM::ASTWalker VirtualMachine;

	class Func;

	class Value
	{
	public:
		enum Type
		{
			NONE,
			NUM,
			BOOL,
			STRING,
			ARRAY,
			MAP,
		};

		Value();

		explicit Value(int64_t value);

		explicit Value(double value);

		explicit Value(bool value);

		explicit Value(const std::string &value);

		Value(const Value &) = delete;
		Value(const Value &&) = delete;
		Value &operator=(const Value &) = delete;
		Value &operator=(Value &&) = delete;

		~Value();

		Value *make_copy() const;

		std::string to_string() const;

		double as_number() const;

		bool as_boolean() const;

		bool is_truthy() const;
		bool is_falsy() const { return !is_truthy(); }
		bool is_equal(const Value &other) const;

		std::size_t get_size() const;
		const Value *get_value(std::size_t index) const;
		const Value *get_value(const std::string &key) const;

		std::string as_string() const;

		void set_value(int64_t value);

		void set_value(double value);

		void set_value(bool value);

		void set_value(const std::string &value);

		void set_value(ssize_t index, Value *value);

		void set_value(const std::string &key, Value *value);

		bool is_null() const { return m_type == Type::NONE; }

		Type get_type() const { return m_type; }

		void set_vm_id(std::size_t id) { m_vm_id = id; }

		std::size_t get_vm_id() const { return m_vm_id; }

	private:
		union
		{
			double m_as_number;
			bool m_as_boolean;
			char *m_as_string;
		} m_data;

		Type m_type;

		std::size_t m_vm_id = 0;

		std::vector<Value *> m_array;
		std::unordered_map<std::string, Value *> m_map;
	};
};

Interpreter::Value operator+(const Interpreter::Value &lhs, const Interpreter::Value &rhs);
Interpreter::Value operator-(const Interpreter::Value &lhs, const Interpreter::Value &rhs);
Interpreter::Value operator*(const Interpreter::Value &lhs, const Interpreter::Value &rhs);
Interpreter::Value operator/(const Interpreter::Value &lhs, const Interpreter::Value &rhs);

bool operator||(const Interpreter::Value &lhs, const Interpreter::Value &rhs);
bool operator&&(const Interpreter::Value &lhs, const Interpreter::Value &rhs);
bool operator==(const Interpreter::Value &lhs, const Interpreter::Value &rhs);
bool operator!(const Interpreter::Value &v);
