// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "value.h"
#include "func.h"
#include "ast/nodes/expressions.h"
#include "ast/nodes/scope.h"
#include "interpreter/vm/ast_walker/ast_walker.h"

namespace Interpreter
{
	typedef VM::ASTWalker VirtualMachine;
};

namespace Interpreter
{

	Func::~Func()
	{
		delete m_block;
	}

	Value *Func::execute(Interpreter::VirtualMachine *virtual_machine, AST::MapExpression *params) const
	{
		m_block->execute(virtual_machine, params);
		return virtual_machine->get_value(VirtualMachine::SCOPE_REENTRY_VALUE);
	}

};
