// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "value.h"
#include "interpreter/vm/ast_walker/ast_walker.h"

#include <cassert>

namespace Interpreter
{
	typedef VM::ASTWalker VirtualMachine;
};

namespace Interpreter
{
	Value::Value() : m_data(), m_type(Type::NONE) {}

	Value::Value(int64_t value) : m_data(), m_type(Type::NONE)
	{
		set_value(value);
	}

	Value::Value(double value) : m_data(), m_type(Type::NONE)
	{
		set_value(value);
	}

	Value::Value(bool value) : m_data(), m_type(Type::NONE)
	{
		set_value(value);
	}

	Value::Value(const std::string &value) : m_data(), m_type(Type::NONE)
	{
		set_value(value);
	}

	Value::~Value()
	{
		switch (m_type)
		{
		case Type::STRING:
			delete m_data.m_as_string;
			break;
		default:
			break;
		}
	}

	Value *Value::make_copy() const
	{
		switch (m_type)
		{
		case Type::STRING:
			return new Value(std::string(m_data.m_as_string));
		case Type::BOOL:
			return new Value(m_data.m_as_boolean);
		case Type::NUM:
			return new Value(m_data.m_as_number);
		case Type::ARRAY:
		{
			auto new_value = new Value();
			for (ssize_t i = 0; i < get_size(); ++i)
			{
				new_value->set_value(i, m_array[i]->make_copy());
			}
			return new_value;
		}
		case Type::MAP:
		{
			auto new_value = new Value();
			for (const auto &it : m_map)
			{
				const std::string &key = it.first;
				new_value->set_value(key, it.second->make_copy());
			}

			return new_value;
		}
		default:
			return new Value();
		}
	}

	std::size_t Value::get_size() const
	{
		switch (m_type)
		{
		case Type::ARRAY:
			return m_array.size();
		case Type::MAP:
			return m_map.size();
		default:
			return 0;
		}
	}

	const Value *Value::get_value(std::size_t index) const
	{
		if (m_array.size() >= index || index < 0)
		{
			return nullptr;
		}

		auto it = m_array.begin() + static_cast<long>(index);

		return *it;
	}

	const Value *Value::get_value(const std::string &key) const
	{
		auto it = m_map.find(key);
		if (it == m_map.end())
		{
			return nullptr;
		}

		return it->second;
	}

	bool Value::as_boolean() const
	{
		assert(m_type == Type::BOOL);
		return m_data.m_as_boolean;
	}

	bool Value::is_truthy() const
	{
		switch (m_type)
		{
		case Type::STRING:
			return !as_string().empty();
		case Type::BOOL:
			return as_boolean();
		case Type::NUM:
			return as_number() != 0.0f;
		case Type::ARRAY:
			return !m_array.empty();
		case Type::MAP:
			return !m_map.empty();
		default:
			return false;
		}
	}

	bool Value::is_equal(const Value &other) const
	{
		if (m_type != other.m_type)
		{
			return false;
		}

		switch (m_type)
		{
		case Type::STRING:
			return as_string() == other.as_string();
		case Type::BOOL:
			return as_boolean() == other.as_boolean();
		case Type::NUM:
			return as_number() == other.as_number();
		case Type::ARRAY:
			return false;
		case Type::MAP:
			return false;
		default:
			return true;
		}
	}

	double Value::as_number() const
	{
		assert(m_type == Type::NUM);
		return m_data.m_as_number;
	}

	std::string Value::as_string() const
	{
		assert(m_type == Type::STRING);
		return std::string(m_data.m_as_string);
	}

	void Value::set_value(int64_t value)
	{
		m_data.m_as_number = static_cast<double>(value);
		m_type = Type::NUM;
	}

	void Value::set_value(double value)
	{
		m_data.m_as_number = value;
		m_type = Type::NUM;
	}

	void Value::set_value(bool value)
	{
		m_data.m_as_boolean = value;
		m_type = Type::BOOL;
	}

	void Value::set_value(const std::string &value)
	{
		char *buffer = new char[value.size() + 1];
		std::copy(value.begin(), value.end(), buffer);
		buffer[value.size()] = '\0';
		m_data.m_as_string = buffer;
		m_type = Type::STRING;
	}

	void Value::set_value(ssize_t index, Value *value)
	{
		m_type = Type::ARRAY;
		if (index == -1)
		{
			m_array.push_back(value);
			return;
		}

		if (m_array.size() >= index || index < 0)
		{
			return;
		}

		m_array.insert(m_array.begin() + static_cast<long>(index), value);
	}

	void Value::set_value(const std::string &key, Value *value)
	{
		m_type = Type::MAP;
		auto it = m_map.find(key);
		if (it != m_map.end())
		{
			m_map.erase(it);
		}

		m_map.insert(std::make_pair(key, value));
	}

	std::string Value::to_string() const
	{
		switch (m_type)
		{
		case Type::STRING:
			return std::string(m_data.m_as_string);
		case Type::BOOL:
			if (m_data.m_as_boolean)
			{
				return "TRUE";
			}
			return "FALSE";
		case Type::NUM:
			return std::to_string(m_data.m_as_number);
		case Type::ARRAY:
		{
			std::string array_str = "(";
			for (const auto &value : m_array)
			{
				if (array_str != "(")
				{
					array_str += ", ";
				}

				array_str += value->to_string();
			}

			array_str += ")";
			return array_str;
		}
		case Type::MAP:
		{
			std::string map_str = "(";
			for (const auto &it : m_map)
			{
				if (map_str != "(")
				{
					map_str += ", ";
				}

				map_str += it.first;
				map_str += ": ";
				map_str += it.second->to_string();
			}

			map_str += ")";
			return map_str;
		}
		default:
			return "NONE";
		}
	}
}

Interpreter::Value operator+(const Interpreter::Value &lhs, const Interpreter::Value &rhs)
{
	assert(lhs.get_type() == Interpreter::Value::Type::NUM && rhs.get_type() == Interpreter::Value::Type::NUM);
	return Interpreter::Value(lhs.as_number() + rhs.as_number());
}

Interpreter::Value operator-(const Interpreter::Value &lhs, const Interpreter::Value &rhs)
{
	assert(lhs.get_type() == Interpreter::Value::Type::NUM && rhs.get_type() == Interpreter::Value::Type::NUM);
	return Interpreter::Value(lhs.as_number() - rhs.as_number());
}

Interpreter::Value operator*(const Interpreter::Value &lhs, const Interpreter::Value &rhs)
{
	assert(lhs.get_type() == Interpreter::Value::Type::NUM && rhs.get_type() == Interpreter::Value::Type::NUM);
	return Interpreter::Value(lhs.as_number() * rhs.as_number());
}

Interpreter::Value operator/(const Interpreter::Value &lhs, const Interpreter::Value &rhs)
{
	assert(lhs.get_type() == Interpreter::Value::Type::NUM && rhs.get_type() == Interpreter::Value::Type::NUM);
	return Interpreter::Value(lhs.as_number() / rhs.as_number());
}

bool operator||(const Interpreter::Value &lhs, const Interpreter::Value &rhs)
{
	return lhs.is_truthy() || rhs.is_truthy();
}

bool operator&&(const Interpreter::Value &lhs, const Interpreter::Value &rhs)
{
	return lhs.is_truthy() && rhs.is_truthy();
}

bool operator==(const Interpreter::Value &lhs, const Interpreter::Value &rhs)
{
	return lhs.is_equal(rhs);
}

bool operator!(const Interpreter::Value &v)
{
	return !v.is_truthy();
}
