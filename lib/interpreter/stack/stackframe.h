// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <cstdint>
#include <map>
#include <string>

namespace Interpreter
{

	class Value;

	namespace VM
	{

		typedef std::size_t value_id;

		class StackFrame
		{
		public:
			StackFrame() = default;

			StackFrame(const StackFrame &) = delete;
			StackFrame(const StackFrame &&) = delete;
			StackFrame &operator=(const StackFrame &) = delete;
			StackFrame &operator=(StackFrame &&) = delete;

			~StackFrame();

			// NOTE: The delegated owner of every Value from the VM
			void set_data(std::string identifier, Value *value);

			Value *get_data(std::string identifier);

			value_id identifier_to_id(std::string identifier) const;

		private:
			std::map<value_id, Value *> m_data;
			std::map<std::string, value_id> m_names;

			static value_id current_value_id;
		};

	};

};
