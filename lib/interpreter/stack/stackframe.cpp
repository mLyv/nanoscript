// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "stackframe.h"

#include "interpreter/data_handles/value.h"
#include "interpreter/vm/ast_walker/ast_walker.h"

#include <cassert>

namespace Interpreter::VM
{

	value_id StackFrame::current_value_id = 100;

	StackFrame::~StackFrame()
	{
		for (const auto &entry : m_data)
		{
			delete entry.second;
		}
	}

	value_id StackFrame::identifier_to_id(std::string identifier) const
	{
		if (m_names.contains(identifier))
		{
			return m_names.at(identifier);
		}

		return 0;
	}

	void StackFrame::set_data(std::string identifier, Value *value)
	{
		value_id id = identifier_to_id(identifier);

		if (id == 0)
		{
			if (value->get_vm_id() != 0)
			{
				id = value->get_vm_id();
			}
			else
			{
				id = current_value_id++;
				value->set_vm_id(id);
			}

			m_names.insert(std::make_pair(identifier, id));
		}

		if (id != value->get_vm_id())
		{
			value->set_vm_id(id);
		}

		if (m_data.contains(id))
		{
			delete m_data.at(id); // Free before discard

			m_data[id] = value;
			return;
		}

		m_data.insert(std::make_pair(id, value));
	}

	Value *StackFrame::get_data(std::string identifier)
	{
		value_id id = identifier_to_id(identifier);
		return m_data.at(id);
	}

};
