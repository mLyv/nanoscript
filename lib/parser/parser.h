// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "lexer/token.h"

#include <cstdint>
#include <utility>

namespace REPL
{

	class REPL;

};

namespace AST
{

	class Scope;
	class AggregateStatement;
	class Expression;

};

namespace Parser
{

	class Parser
	{
	public:
		Parser(std::vector<Lexer::Token> tokens) : m_tokens(std::move(tokens)){};

		Parser(const Parser &) = delete;
		Parser(const Parser &&) = delete;
		Parser &operator=(const Parser &) = delete;
		Parser &operator=(Parser &&) = delete;

		~Parser() = default;

		AST::Scope *parse() const;

	private:
		AST::Scope *process_scope(std::size_t token_start, std::size_t token_end) const;

		AST::AggregateStatement *process_block(std::size_t token_start, std::size_t token_end) const;

		AST::Expression *process_expression(const std::vector<Lexer::Token> &tokens) const;

		AST::Expression *process_expression(const std::string &term) const;

		std::vector<Lexer::Token> m_tokens;

		friend class REPL::REPL;
	};

};
