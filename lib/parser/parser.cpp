// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "parser.h"

#include "ast/nodes/expressions.h"
#include "ast/nodes/scope.h"
#include "ast/nodes/statements.h"
#include "interpreter/data_handles/value.h"

#include <unordered_map>

namespace Parser
{

	// TODO: Remove all the duplication between this and process_scope
	AST::AggregateStatement *Parser::process_block(std::size_t token_start, std::size_t token_end) const
	{
		AST::AggregateStatement *block = new AST::AggregateStatement();

		for (std::size_t i = token_start; i <= token_end; ++i)
		{
			const Lexer::Token &token = m_tokens[i];

			if (token.get_type() == Lexer::Token::Type::RETURN)
			{
				AST::ReturnStatement *ret_statement = new AST::ReturnStatement(
					process_expression(token.get_expression()));
				block->append(ret_statement);
				continue;
			}

			if (token.get_type() == Lexer::Token::Type::STORE)
			{
				AST::AssignStatement *assign_statement = new AST::AssignStatement(token.get_name(), process_expression(
																										token.get_expression()));
				block->append(assign_statement);
				continue;
			}

			if (token.get_type() == Lexer::Token::Type::CONDITION)
			{
				// expression
				AST::Expression *expression = process_expression(token.get_expression());

				// then
				std::size_t then_block_open = i + 1;
				std::size_t then_block_exit = then_block_open;

				while (m_tokens[then_block_exit].get_type() != Lexer::Token::Type::BLOCK_EXIT)
				{
					then_block_exit++;
				}
				AST::Statement *then_block = process_block(then_block_open, then_block_exit);

				// else
				std::size_t else_block_open = i + 1;
				std::size_t else_block_exit = else_block_open;

				while (m_tokens[else_block_exit].get_type() != Lexer::Token::Type::BLOCK_EXIT)
				{
					else_block_exit++;
				}
				AST::Statement *else_block = process_block(else_block_open, else_block_exit);

				AST::ConditionalStatement *conditional_statement = new AST::ConditionalStatement(expression, then_block, else_block);
				block->append(conditional_statement);

				i = else_block_exit;
				continue;
			}
		}

		return block;
	}

	AST::Scope *Parser::process_scope(std::size_t token_start, std::size_t token_end) const
	{
		AST::Scope *scope = new AST::Scope();

		for (std::size_t i = token_start; i <= token_end; ++i)
		{
			const Lexer::Token &token = m_tokens[i];
			if (token.get_type() == Lexer::Token::Type::FUNCTION)
			{
				std::string function_name = token.get_name();

				std::size_t scope_open = i + 1;
				std::size_t scope_exit = scope_open;

				while (m_tokens[scope_exit].get_type() != Lexer::Token::Type::SCOPE_EXIT)
				{
					scope_exit++;
				}

				AST::Scope *function_body = process_scope(scope_open, scope_exit);

				scope->append(new AST::FunctionDeclarationStatement(function_body, function_name));

				i = scope_exit;
				continue;
			}

			if (token.get_type() == Lexer::Token::Type::RETURN)
			{
				AST::ReturnStatement *ret_statement = new AST::ReturnStatement(
					process_expression(token.get_expression()));
				scope->append(ret_statement);
				continue;
			}

			if (token.get_type() == Lexer::Token::Type::STORE)
			{
				AST::AssignStatement *assign_statement = new AST::AssignStatement(token.get_name(), process_expression(
																										token.get_expression()));
				scope->append(assign_statement);
				continue;
			}

			if (token.get_type() == Lexer::Token::Type::CONDITION)
			{
				// expression
				AST::Expression *expression = process_expression(token.get_expression());

				// then
				std::size_t then_block_open = i + 1;
				std::size_t then_block_exit = then_block_open;

				while (m_tokens[then_block_exit].get_type() != Lexer::Token::Type::BLOCK_EXIT)
				{
					then_block_exit++;
				}
				AST::Statement *then_block = process_block(then_block_open, then_block_exit);

				// else
				std::size_t else_block_open = then_block_exit + 1;
				std::size_t else_block_exit = else_block_open;

				while (m_tokens[else_block_exit].get_type() != Lexer::Token::Type::BLOCK_EXIT)
				{
					else_block_exit++;
				}
				AST::Statement *else_block = process_block(else_block_open, else_block_exit);

				AST::ConditionalStatement *conditional_statement = new AST::ConditionalStatement(expression, then_block, else_block);
				scope->append(conditional_statement);

				i = else_block_exit;
				continue;
			}

			if (token.get_type() == Lexer::Token::Type::LOOP)
			{
				AST::Expression *expression = process_expression(token.get_expression());

				std::size_t block_open = i + 1;
				std::size_t block_exit = block_open;

				while (m_tokens[block_exit].get_type() != Lexer::Token::Type::BLOCK_EXIT)
				{
					block_exit++;
				}

				AST::Statement *block = process_block(block_open, block_exit);

				AST::LoopStatement *loop_statement = new AST::LoopStatement(expression, block);
				scope->append(loop_statement);

				i = block_exit;
				continue;
			}
		}

		return scope;
	}

	AST::Expression *Parser::process_expression(const std::vector<Lexer::Token> &tokens) const
	{
		if (tokens.size() == 1)
		{
			return process_expression(tokens[0].get_name());
		}

		if (tokens[0].get_type() == Lexer::Token::MAP_OPEN && tokens[tokens.size() - 1].get_type() == Lexer::Token::MAP_EXIT)
		{
			std::unordered_map<std::string, AST::Expression *> map_elements;

			for (auto i = 1; i < tokens.size() - 1; ++i)
			{
				AST::Expression *exp = process_expression(tokens[i].get_expression());
				map_elements.insert(std::make_pair(tokens[i].get_name(), exp));
			}

			return new AST::MapExpression(map_elements);
		}

		if (tokens[0].get_type() == Lexer::Token::LIST_OPEN && tokens[tokens.size() - 1].get_type() == Lexer::Token::LIST_EXIT)
		{
			std::vector<AST::Expression *> list_elements;

			for (auto i = 1; i < tokens.size() - 1; ++i)
			{
				AST::Expression *exp = process_expression(tokens[i].get_name());
				list_elements.push_back(exp);
			}

			return new AST::ListExpression(list_elements);
		}

		bool is_binary = tokens.size() == 3;
		if (is_binary)
		{
			const Lexer::Token &left_token = tokens[0];
			const Lexer::Token &op_token = tokens[1];
			const Lexer::Token &right_token = tokens[2];

			AST::BinaryExpression::Operator op;
			if (op_token.get_name() == "+")
			{
				op = AST::BinaryExpression::Operator::ADD;
			}
			else if (op_token.get_name() == "-")
			{
				op = AST::BinaryExpression::Operator::SUB;
			}
			else if (op_token.get_name() == "*")
			{
				op = AST::BinaryExpression::Operator::MUL;
			}
			else if (op_token.get_name() == "/")
			{
				op = AST::BinaryExpression::Operator::DIV;
			}
			else if (op_token.get_name() == "&&")
			{
				op = AST::BinaryExpression::Operator::AND;
			}
			else if (op_token.get_name() == "||")
			{
				op = AST::BinaryExpression::Operator::OR;
			}
			else if (op_token.get_name() == "==")
			{
				op = AST::BinaryExpression::Operator::EQUAL;
			}
			else if (op_token.get_name() == "!=")
			{
				op = AST::BinaryExpression::Operator::NOTEQUAL;
			}
			else if (op_token.get_name() == ">")
			{
				op = AST::BinaryExpression::Operator::GREATER;
			}
			else if (op_token.get_name() == "<")
			{
				op = AST::BinaryExpression::Operator::LESSER;
			}
			else if (op_token.get_name() == ">=")
			{
				op = AST::BinaryExpression::Operator::GREATEREQUAL;
			}
			else if (op_token.get_name() == "<=")
			{
				op = AST::BinaryExpression::Operator::LESSEREQUAL;
			}
			else
			{
				return nullptr;
			}

			AST::Expression *left = process_expression(left_token.get_name());
			AST::Expression *right = process_expression(right_token.get_name());

			return new AST::BinaryExpression(left, right, op);
		}

		const Lexer::Token &call_token = tokens[0];
		const Lexer::Token &func_token = tokens[1];

		if (call_token.get_name() != "call")
		{
			return nullptr;
		}

		return new AST::CallExpression(func_token.get_name(), dynamic_cast<AST::MapExpression *>(process_expression(call_token.get_expression())));
	}

	AST::Expression *Parser::process_expression(const std::string &term) const
	{
		// Is string literal
		if (term[0] == '"')
		{
			std::string str = "";
			for (std::size_t i = 1; i < term.size() - 1; ++i)
			{
				str += term[i];
			}

			return new AST::LiteralExpression(new Interpreter::Value(str));
		}

		bool is_number = true;
		bool found_decimal = false;
		for (const char &c : term)
		{
			if (!std::isdigit(c) && c != '.')
			{
				is_number = false;
				break;
			}

			if (c == '.')
			{
				if (!found_decimal)
				{
					found_decimal = true;
					continue;
				}
				else
				{
					is_number = false;
					break;
				}
			}
		}

		if (is_number)
		{
			return new AST::LiteralExpression(new Interpreter::Value(std::stod(term)));
		}

		return new AST::StoredExpression(term);
	}

	AST::Scope *Parser::parse() const
	{
		AST::Scope *program = process_scope(0, m_tokens.size() - 1);

		// The final statement in all programs
		program->append(new AST::ReturnStatement(new AST::CallExpression("main", new AST::MapExpression({}))));

		return program;
	}

};
