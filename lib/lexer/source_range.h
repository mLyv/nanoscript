// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <string>

namespace Lexer
{

	struct SourceRange
	{
		SourceRange(const char *str) : source(str), start_pos(0) {}
		SourceRange(const std::string &str) : source(str), start_pos(0) {}
		SourceRange(const std::string &str, size_t start) : source(str), start_pos(start) {}

		std::string source;
		size_t start_pos;

		const char &operator[](size_t index) const;
		std::string::const_iterator begin() const;
		std::string::const_iterator end() const;
		size_t size() const;
		SourceRange substr(size_t start, size_t end) const;
		size_t find_first_of(char c) const;
		size_t find_first_of(char c, size_t from) const;
		size_t find(char c) const;
		size_t find(const char *str) const;
		bool empty() const;
	};
}
