// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "source_range.h"

namespace Lexer
{

	const char &SourceRange::operator[](size_t index) const
	{
		return source[index];
	}

	std::string::const_iterator SourceRange::begin() const
	{
		return source.begin();
	}

	std::string::const_iterator SourceRange::end() const
	{
		return source.end();
	}

	size_t SourceRange::size() const
	{
		return source.size();
	}

	SourceRange SourceRange::substr(size_t start, size_t end) const
	{
		return {source.substr(start, end), start};
	}

	size_t SourceRange::find_first_of(char c) const
	{
		return source.find_first_of(c);
	}

	size_t SourceRange::find_first_of(char c, size_t from) const
	{
		return source.find_first_of(c, from);
	}

	size_t SourceRange::find(char c) const
	{
		return source.find(c);
	}

	size_t SourceRange::find(const char *str) const
	{
		return source.find(str);
	}

	bool SourceRange::empty() const
	{
		return source.empty();
	}

};
