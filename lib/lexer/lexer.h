// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "grammar/keywords.h"
#include "source_range.h"
#include "token.h"

#include <string>
#include <string_view>
#include <utility>
#include <vector>

namespace Lexer
{
	class Lexer
	{
	public:
		explicit Lexer(std::string source_code) : m_source_code(std::move(source_code)),
												  m_rule_book(new Grammar::RuleBook()){};

		Lexer(const Lexer &) = delete;
		Lexer(const Lexer &&) = delete;
		Lexer &operator=(const Lexer &) = delete;
		Lexer &operator=(Lexer &&) = delete;

		~Lexer();

		std::vector<Token> parse() const;

	private:
		std::string m_source_code;
		Grammar::RuleBook *m_rule_book;

		std::vector<std::pair<std::string, SourceRange>> extract_statements(SourceRange scope) const;

		void process_statements(std::vector<Token> &tokens,
								const std::vector<std::pair<std::string, SourceRange>> &statements) const;

		void tokenize_function_statement(std::vector<Token> &tokens, SourceRange statement) const;

		void tokenize_store_statement(std::vector<Token> &tokens, SourceRange statement) const;

		void tokenize_return_statement(std::vector<Token> &tokens, SourceRange statement) const;

		void tokenize_condition_statement(std::vector<Token> &tokens, SourceRange statement) const;

		void tokenize_loop_statement(std::vector<Token> &tokens, SourceRange statement) const;

		void tokenize_expression(std::vector<Token> &tokens, SourceRange expression) const;
	};

};
