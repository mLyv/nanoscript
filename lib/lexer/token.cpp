// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "token.h"

namespace Lexer
{

	std::string Token::to_string() const
	{
		std::string ret;
		std::string type;

		switch (m_type)
		{
		case Type::FUNCTION:
			type = "FUNCTION";
			break;
		case Type::SCOPE_OPEN:
			type = "SCOPE_OPEN";
			break;
		case Type::SCOPE_EXIT:
			type = "SCOPE_EXIT";
			break;
		case Type::BLOCK_OPEN:
			type = "BLOCK_OPEN";
			break;
		case Type::BLOCK_EXIT:
			type = "BLOCK_EXIT";
			break;
		case Type::STORE:
			type = "STORE";
			break;
		case Type::RETURN:
			type = "RETURN";
			break;
		case Type::CONDITION:
			type = "CONDITION";
			break;
		case Type::LOOP:
			type = "LOOP";
			break;
		case Type::TERM:
			type = "TERM";
			break;
		case Type::OPERATOR:
			type = "OPERATOR";
			break;
		case Type::LIST_OPEN:
			type = "LIST_OPEN";
			break;
		case Type::LIST_EXIT:
			type = "LIST_EXIT";
			break;
		case Type::MAP_OPEN:
			type = "MAP_OPEN";
			break;
		case Type::MAP_KEY:
			type = "MAP_KEY";
			break;
		case Type::MAP_EXIT:
			type = "MAP_EXIT";
			break;
		default:
			type = "UNKNOWN";
			break;
		}

		ret += type;

		if (!m_name.empty())
		{
			ret += " - " + m_name.source;
		}

		if (!m_expression.empty())
		{
			ret += " {";
		}

		for (const auto &value : m_expression)
		{
			ret += " " + value.to_string() + " ";
		}

		if (!m_expression.empty())
		{
			ret += "} ";
		}

		return ret;
	}

};
