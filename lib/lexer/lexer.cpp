// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "lexer.h"

#include "token.h"
#include "grammar/keywords.h"

#include <cctype>

namespace Lexer
{

	Lexer::~Lexer()
	{
		delete m_rule_book;
	}

	std::vector<std::pair<std::string, SourceRange>> Lexer::extract_statements(SourceRange scope) const
	{
		std::vector<std::pair<std::string, SourceRange>> extracted_statements;

		std::string current_word;
		std::size_t current_word_start = 0;
		for (std::size_t i = 0; i < scope.size(); ++i)
		{
			const char &c = scope[i];
			if (c == '\n' || c == '\t')
			{
				current_word = "";
				current_word_start = i + 1;
				continue;
			}

			if (c == '@')
			{
				std::size_t new_line = i + 1;

				while (scope[new_line] != '\n')
				{
					new_line++;
				}

				i = new_line;

				current_word = "";
				current_word_start = i;
				continue;
			}

			if (c == ' ')
			{
				const std::string &statement = m_rule_book->get_statement(current_word);
				if (statement == "unknown")
				{
					extracted_statements.emplace_back("unknown", "");
					return extracted_statements; // ERROR
				}

				if (statement == "function")
				{
					std::size_t function_close_offset = i + 1;
					while (scope[function_close_offset] != ']')
					{
						function_close_offset++;
					}

					i = ++function_close_offset;

					extracted_statements.emplace_back("function",
													  scope.substr(current_word_start, function_close_offset - current_word_start));

					current_word = "";
					current_word_start = i;
					continue;
				}

				if (statement == "condition")
				{
					std::size_t condition_close_offset = i + 1;
					std::size_t open_blocks = 0;
					while (true)
					{
						if (scope[condition_close_offset] == ' ')
						{
							condition_close_offset++;
							continue;
						}

						if (scope[condition_close_offset] == '{')
						{
							open_blocks++;
						}
						else if (scope[condition_close_offset] == '}')
						{
							if (open_blocks == 2)
							{
								break;
							}
						}

						condition_close_offset++;
					}

					i = ++condition_close_offset;

					extracted_statements.emplace_back("condition", scope.substr(current_word_start, condition_close_offset - current_word_start));

					current_word = "";
					current_word_start = i;
					continue;
				}

				if (statement == "loop")
				{
					std::size_t loop_close_offset = i + 1;
					while (scope[loop_close_offset] != '}')
					{
						loop_close_offset++;
					}

					i = ++loop_close_offset;
					extracted_statements.emplace_back("loop", scope.substr(current_word_start, loop_close_offset - current_word_start));

					current_word = "";
					current_word_start = i;
					continue;
				}

				// NOTE: All other statements are ; terminated and ; can only occur at the end
				std::size_t semicolon_terminated_statement_close_offset = i + 1;
				while (scope[semicolon_terminated_statement_close_offset] != ';')
				{
					semicolon_terminated_statement_close_offset++;
				}

				i = ++semicolon_terminated_statement_close_offset;
				extracted_statements.emplace_back(statement, scope.substr(current_word_start,
																		  semicolon_terminated_statement_close_offset - current_word_start));

				current_word = "";
				current_word_start = i;
			}
			else
			{
				current_word += c;
			}
		}

		return extracted_statements;
	}

	void Lexer::process_statements(std::vector<Token> &tokens,
								   const std::vector<std::pair<std::string, SourceRange>> &statements) const
	{
		for (const auto &statement : statements)
		{
			if (statement.first == "function")
			{
				tokenize_function_statement(tokens, statement.second);
				continue;
			}

			if (statement.first == "store")
			{
				tokenize_store_statement(tokens, statement.second);
				continue;
			}

			if (statement.first == "return")
			{
				tokenize_return_statement(tokens, statement.second);
				continue;
			}

			if (statement.first == "condition")
			{
				tokenize_condition_statement(tokens, statement.second);
				continue;
			}

			if (statement.first == "loop")
			{
				tokenize_loop_statement(tokens, statement.second);
				continue;
			}
		}
	}

	void Lexer::tokenize_function_statement(std::vector<Token> &tokens, SourceRange statement) const
	{
		std::size_t first_index = statement.find_first_of(' ');
		std::size_t second_index = statement.find_first_of(' ', first_index + 1);
		SourceRange function_name = statement.substr(first_index + 1, (second_index - first_index) - 1);

		tokens.emplace_back(Token::Type::FUNCTION, function_name);
		tokens.emplace_back(Token::Type::SCOPE_OPEN, "");

		first_index = statement.find_first_of('[');
		second_index = statement.find_first_of(']');
		SourceRange function_body = statement.substr(first_index + 1, (second_index - first_index) - 1);
		std::vector<std::pair<std::string, SourceRange>> extracted_statements = extract_statements(function_body);
		process_statements(tokens, extracted_statements);

		tokens.emplace_back(Token::Type::SCOPE_EXIT, "");
	}

	void Lexer::tokenize_store_statement(std::vector<Token> &tokens, SourceRange statement) const
	{
		std::size_t first_index = statement.find_first_of(' ');
		std::size_t second_index = statement.find_first_of(' ', first_index + 1);
		SourceRange identifier = statement.substr(first_index + 1, (second_index - 1) - first_index);

		first_index = statement.find_first_of('=');
		second_index = statement.find_first_of(';', first_index);
		SourceRange expression = statement.substr(first_index + 2, (second_index - 2) - first_index);
		std::vector<Token> expression_tokenized;
		tokenize_expression(expression_tokenized, expression);

		tokens.emplace_back(Token::Type::STORE, identifier, expression_tokenized);
	}

	void Lexer::tokenize_return_statement(std::vector<Token> &tokens, SourceRange statement) const
	{
		std::size_t first_index = statement.find_first_of(' ');
		std::size_t second_index = statement.find_first_of(';');
		SourceRange expression = statement.substr(first_index + 1, (second_index - 1) - first_index);
		std::vector<Token> expression_tokenized;
		tokenize_expression(expression_tokenized, expression);

		tokens.emplace_back(Token::Type::RETURN, "", expression_tokenized);
	}

	void Lexer::tokenize_loop_statement(std::vector<Token> &tokens, SourceRange statement) const
	{
		std::size_t first_index = statement.find_first_of(' ');
		std::size_t second_index = statement.find('{');
		SourceRange expression = statement.substr(first_index + 1, (second_index - 2) - first_index);
		std::vector<Token> expression_tokenized;
		tokenize_expression(expression_tokenized, expression);
		tokens.emplace_back(Token::Type::LOOP, "", expression_tokenized);

		tokens.emplace_back(Token::Type::BLOCK_OPEN, "");
		first_index = statement.find_first_of('{');
		second_index = statement.find_first_of('}');
		SourceRange block = statement.substr(first_index + 1, (second_index - 1) - first_index);
		std::vector<std::pair<std::string, SourceRange>> extracted_statements = extract_statements(block);
		process_statements(tokens, extracted_statements);
		tokens.emplace_back(Token::Type::BLOCK_EXIT, "");
	}

	void Lexer::tokenize_condition_statement(std::vector<Token> &tokens, SourceRange statement) const
	{
		std::size_t first_index = statement.find_first_of(' ');
		std::size_t second_index = statement.find("then");
		SourceRange expression = statement.substr(first_index + 1, (second_index - 2) - first_index);
		std::vector<Token> expression_tokenized;
		tokenize_expression(expression_tokenized, expression);
		tokens.emplace_back(Token::Type::CONDITION, "", expression_tokenized);

		tokens.emplace_back(Token::Type::BLOCK_OPEN, "then");
		first_index = statement.find_first_of('{');
		second_index = statement.find_first_of('}');
		SourceRange then_block = statement.substr(first_index + 1, (second_index - 1) - first_index);
		std::vector<std::pair<std::string, SourceRange>> extracted_then_statements = extract_statements(then_block);
		process_statements(tokens, extracted_then_statements);
		tokens.emplace_back(Token::Type::BLOCK_EXIT, "");

		tokens.emplace_back(Token::Type::BLOCK_OPEN, "else");
		first_index = statement.find_first_of('{', first_index + 1);
		second_index = statement.find_first_of('}', first_index);
		if (first_index != std::string::npos && second_index != std::string::npos)
		{
			SourceRange else_block = statement.substr(first_index + 1, (second_index - 1) - first_index);
			std::vector<std::pair<std::string, SourceRange>> extracted_else_statements = extract_statements(else_block);
			process_statements(tokens, extracted_else_statements);
		}

		tokens.emplace_back(Token::Type::BLOCK_EXIT, "");
	}

	void Lexer::tokenize_expression(std::vector<Token> &tokens, SourceRange expression) const
	{
		std::string current_value;
		std::vector<SourceRange> extracted_terms;

		// list / map declaration expression
		if (expression[0] == '(' && expression[expression.size() - 1] == ')')
		{
			// list
			if (expression.find(':') == std::string::npos)
			{
				tokens.emplace_back(Token::Type::LIST_OPEN, "");
				SourceRange list_terms = expression.substr(1, expression.size() - 2);
				for (const char &c : list_terms)
				{
					if (c == ' ')
					{
						continue;
					}

					if (c == ',')
					{
						tokenize_expression(tokens, {current_value, 0});
						current_value = "";
						continue;
					}

					current_value += c;
				}

				if (!current_value.empty())
				{
					tokenize_expression(tokens, {current_value, 0});
					current_value = "";
				}

				tokens.emplace_back(Token::Type::LIST_EXIT, "");
			}
			else
			{ // map
				tokens.emplace_back(Token::Type::MAP_OPEN, "");
				SourceRange map_terms = expression.substr(1, expression.size() - 2);
				bool parsing_key = true;
				bool parsing_value = false;
				std::string key_name;
				for (const char &c : map_terms)
				{
					if (!parsing_value && c == ' ')
					{
						parsing_value = true;
						continue;
					}

					if (c == ',')
					{
						parsing_key = true;
						parsing_value = false;

						std::vector<Token> value_expression_tokenized;
						tokenize_expression(value_expression_tokenized, {current_value, 0});
						tokens.emplace_back(Token::Type::MAP_KEY, key_name, value_expression_tokenized);

						current_value = "";
						key_name = "";
						continue;
					}

					if (c == ':')
					{
						parsing_key = false;
						continue;
					}

					if (parsing_key)
					{
						key_name += c;
						continue;
					}

					current_value += c;
				}

				if (!current_value.empty())
				{
					std::vector<Token> value_expression_tokenized;
					tokenize_expression(value_expression_tokenized, {current_value, 0});
					tokens.emplace_back(Token::Type::MAP_KEY, key_name, value_expression_tokenized);

					current_value = "";
					key_name = "";
				}

				tokens.emplace_back(Token::Type::MAP_EXIT, "");
			}

			return;
		}

		bool is_map_or_list_accessor = false;
		ssize_t open_braces_within = 0;
		bool is_string = false;

		for (const char &c : expression)
		{
			if (is_map_or_list_accessor)
			{
				if (c == '(')
				{
					open_braces_within++;
				}

				if (c == ')')
				{
					open_braces_within--;

					if (open_braces_within < 0)
					{
						is_map_or_list_accessor = false;
						extracted_terms.emplace_back(current_value + ')');
						current_value = "";
					}
					else
					{
						current_value += c;
					}
				}
				else
				{
					current_value += c;
				}

				continue;
			}

			if (is_string)
			{
				if (c == '"')
				{
					is_string = false;
					extracted_terms.emplace_back(current_value + '"');
					current_value = "";
				}
				else
				{
					current_value += c;
				}

				continue;
			}

			if (c == ' ')
			{
				if (!current_value.empty())
				{
					extracted_terms.emplace_back(current_value);
					current_value = "";
				}

				continue;
			}

			if (c == '(')
			{
				is_map_or_list_accessor = true;
				current_value += "(";
				continue;
			}

			if (c == '"')
			{
				is_string = true;
				current_value = "\"";
				continue;
			}

			current_value += c;
		}

		if (!current_value.empty())
		{
			extracted_terms.emplace_back(current_value);
		}

		if (extracted_terms[0].source == "call")
		{
			std::string params;
			for (auto i = 3; i < extracted_terms.size(); ++i)
			{
				params += extracted_terms[i].source;
			}

			// 0 -> call, 1 -> func name, 2 -> with, 3 to size() -> params

			std::vector<Token> param_map;
			tokenize_expression(param_map, {params, 0});

			tokens.emplace_back(Token::Type::TERM, extracted_terms[0], param_map);
			tokens.emplace_back(Token::Type::TERM, extracted_terms[1]);

			return;
		}

		for (const SourceRange &term_range : extracted_terms)
		{
			const std::string &term = term_range.source;
			if (term == "+" || term == "-" || term == "/" || term == "*" || term == "&&" || term == "||" || term == "==" || term == "!=" || term == ">" || term == "<" || term == ">=" || term == "<=")
			{
				tokens.emplace_back(Token::Type::OPERATOR, term);
			}
			else
			{
				tokens.emplace_back(Token::Type::TERM, term);
			}
		}
	}

	std::vector<Token> Lexer::parse() const
	{
		std::vector<Token> tokens;

		std::vector<std::pair<std::string, SourceRange>> extracted_statements = extract_statements(
			{m_source_code, 0});
		process_statements(tokens, extracted_statements);

		return tokens;
	}

};
