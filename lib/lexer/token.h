// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "source_range.h"

#include <string>
#include <utility>
#include <vector>

namespace Lexer
{

	class Token
	{
	public:
		enum Type
		{
			// Statements
			FUNCTION,
			SCOPE_OPEN,
			SCOPE_EXIT,
			STORE,
			RETURN,
			CONDITION,
			LOOP,
			BLOCK_OPEN,
			BLOCK_EXIT,

			// Expressions
			OPERATOR,
			TERM,
			LIST_OPEN,
			LIST_EXIT,
			MAP_OPEN,
			MAP_KEY,
			MAP_EXIT
		};

		Token(Type type, SourceRange name) : m_type(type), m_name(name), m_expression(std::vector<Token>()) {}

		Token(Type type, SourceRange name, std::vector<Token> expression) : m_type(type), m_name(name),
																			m_expression(std::move(expression)) {}

		Token(Type type, const std::string &name) : m_type(type), m_name(name), m_expression(std::vector<Token>()) {}

		Token(Type type, const std::string &name, std::vector<Token> expression) : m_type(type), m_name(name),
																				   m_expression(std::move(expression)) {}

		Token(Type type, const char *name) : m_type(type), m_name(name), m_expression(std::vector<Token>()) {}

		Token(Type type, const char *name, std::vector<Token> expression) : m_type(type), m_name(name),
																			m_expression(std::move(expression)) {}

		~Token() = default;

		std::string to_string() const;

		Type get_type() const { return m_type; };

		std::string get_name() const { return m_name.source; };

		const std::vector<Token> &get_expression() const { return m_expression; };

	private:
		Type m_type;

		SourceRange m_name;
		std::vector<Token> m_expression;
	};

};
