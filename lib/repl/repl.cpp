// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "repl.h"
#include "interpreter/vm/ast_walker/ast_walker.h"
#include "lexer/lexer.h"
#include "parser/parser.h"
#include "ast/nodes/scope.h"
#include "ast/nodes/statements.h"
#include "interpreter/data_handles/value.h"

// REPL does not use Logger::Logger
#include <iostream>

void REPL::REPL::main_loop()
{
	auto rule_book = Lexer::Grammar::RuleBook();
	auto vm = Interpreter::VM::ASTWalker();
	vm.push_stack();

	while (true)
	{
		std::string line;
		std::cout << "\n>>> ";
		if (!std::getline(std::cin, line) || line == "exit")
		{
			break;
		}

		line += ';';

		std::string current_word;
		bool is_exp = false;

		for (std::size_t i = 0; i < line.size(); ++i)
		{
			const char &c = line[i];
			if (c == ' ' || i == line.size() - 1)
			{
				const std::string &statement = rule_book.get_statement(current_word);
				if (statement == "unknown")
				{
					line = "store repl_ret = " + line;
					is_exp = true;
				}

				break;
			}
			else
			{
				current_word += c;
			}
		}

		auto lexer = Lexer::Lexer(line);
		auto tokens = lexer.parse();

		auto parser = Parser::Parser(tokens);
		auto program = parser.process_scope(0, tokens.size() - 1);
		auto statements = program->m_children;

		for (const auto child : statements)
		{
			child->execute(&vm);
		}

		if (vm.has_error())
		{
			vm.reset_error();
		}
		else
		{
			if (is_exp)
			{
				auto repl_ret = vm.get_value("repl_ret");
				std::cout << repl_ret->to_string() << std::endl;
			}
			else
			{
				std::cout << "void" << std::endl;
			}
		}

		delete program;
	}

	vm.pop_stack();
}
