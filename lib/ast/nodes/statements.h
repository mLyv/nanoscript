// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "node.h"

#include <cstdint>
#include <string>
#include <vector>

namespace Interpreter
{
	class Value;

	namespace VM
	{
		class ASTWalker;
	};

	typedef VM::ASTWalker VirtualMachine;

};

namespace AST
{
	class Scope;

	class Expression;

	class Statement : public Node
	{
	public:
		~Statement() override = default;

		virtual void execute(Interpreter::VirtualMachine *virtual_machine) const = 0;

	protected:
		Statement() = default;
	};

	class AggregateStatement final : public Statement
	{
	public:
		explicit AggregateStatement() = default;

		~AggregateStatement() override;

		bool operator==(const Node &) const override;

		void append(Statement *statement);

		void execute(Interpreter::VirtualMachine *virtual_machine) const override;

		void print(std::size_t depth) const override;

	private:
		std::vector<Statement *> m_statements;
	};

	class VoidStatement final : public Statement
	{
	public:
		VoidStatement() = default;

		~VoidStatement() override = default;

		bool operator==(const Node &) const override;

		void execute(Interpreter::VirtualMachine *virtual_machine) const override{};

		void print(std::size_t depth) const override;
	};

	class ReturnStatement final : public Statement
	{
	public:
		explicit ReturnStatement(Expression *exp) : m_exp(exp){};

		~ReturnStatement() override;

		bool operator==(const Node &) const override;

		void execute(Interpreter::VirtualMachine *virtual_machine) const override;

		void print(std::size_t depth) const override;

	private:
		Expression *m_exp;
	};

	class AssignStatement final : public Statement
	{
	public:
		AssignStatement(std::string value_identifier, Expression *new_result) : m_value_identifier(value_identifier),
																				m_exp(new_result){};

		~AssignStatement() override;

		bool operator==(const Node &) const override;

		void execute(Interpreter::VirtualMachine *virtual_machine) const override;

		void print(std::size_t depth) const override;

	private:
		std::string m_value_identifier;
		Expression *m_exp;
	};

	class ConditionalStatement final : public Statement
	{
	public:
		ConditionalStatement(Expression *condition, Statement *true_statement, Statement *false_statement)
			: m_condition(condition), m_true_statement(true_statement), m_false_statement(false_statement){};

		~ConditionalStatement() override;

		bool operator==(const Node &) const override;

		void execute(Interpreter::VirtualMachine *virtual_machine) const override;

		void print(std::size_t depth) const override;

	private:
		Expression *m_condition;
		Statement *m_true_statement;
		Statement *m_false_statement;
	};

	class LoopStatement final : public Statement
	{
	public:
		LoopStatement(Expression *condition, Statement *statement) : m_condition(condition), m_statement(statement){};

		~LoopStatement() override;

		bool operator==(const Node &) const override;

		void execute(Interpreter::VirtualMachine *virtual_machine) const override;

		void print(std::size_t depth) const override;

	private:
		Expression *m_condition;
		Statement *m_statement;
	};

	class FunctionDeclarationStatement final : public Statement
	{
	public:
		FunctionDeclarationStatement(Scope *func_body, std::string name) : m_body(func_body), m_name(name){};

		~FunctionDeclarationStatement() override = default;

		bool operator==(const Node &) const override;

		void execute(Interpreter::VirtualMachine *virtual_machine) const override;

		void print(std::size_t depth) const override;

	private:
		std::string m_name;
		Scope *m_body; // Not owned
	};

}
