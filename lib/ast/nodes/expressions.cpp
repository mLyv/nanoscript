// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "expressions.h"

#include "interpreter/data_handles/func.h"
#include "interpreter/data_handles/value.h"
#include "interpreter/vm/ast_walker/ast_walker.h"
#include "interpreter/vm/exception/exception.h"
#include "logger/logger.h"

#include <cassert>

namespace Interpreter
{
	typedef VM::ASTWalker VirtualMachine;
};

namespace AST
{

	//////
	ListExpression::~ListExpression()
	{
		for (Expression *exp : m_exps)
		{
			delete exp;
		}
	}

	void ListExpression::print(std::size_t depth) const
	{
		std::string tabs(depth, '-');
		LOGGER.Raw(stdout, "%s> %s -\n", tabs.c_str(), "ListExpression");

		for (Expression *exp : m_exps)
		{
			exp->print(depth + 1);
		}
	}

	bool ListExpression::operator==(const Node &other) const
	{
		const ListExpression *other_casted = dynamic_cast<const ListExpression *>(&other);
		if (!other_casted)
		{
			return false;
		}

		if (m_exps.size() != other_casted->m_exps.size())
		{
			return false;
		}

		for (size_t i = 0; i < m_exps.size(); ++i)
		{
			if (*(m_exps[i]) != *(other_casted->m_exps[i]))
			{
				return false;
			}
		}

		return true;
	}

	Interpreter::Value *ListExpression::evaluate(Interpreter::VirtualMachine *virtual_machine) const
	{
		Interpreter::Value *list = new Interpreter::Value();

		for (Expression *exp : m_exps)
		{
			Interpreter::Value *exp_value = exp->evaluate(virtual_machine);
			list->set_value(-1, exp_value);
		}

		return list;
	}

	//////
	MapExpression::~MapExpression()
	{
		for (const auto &it : m_exps)
		{
			delete it.second;
		}
	}

	void MapExpression::print(std::size_t depth) const
	{
		std::string tabs(depth, '-');
		LOGGER.Raw(stdout, "%s> %s -\n", tabs.c_str(), "MapExpression");

		for (const auto &it : m_exps)
		{
			LOGGER.Raw(stdout, "%s> %s -\n", std::string(depth + 1, '-').c_str(), it.first.c_str());
			it.second->print(depth + 2);
		}
	}

	bool MapExpression::operator==(const Node &other) const
	{
		const MapExpression *other_casted = dynamic_cast<const MapExpression *>(&other);
		if (!other_casted)
		{
			return false;
		}

		if (m_exps.size() != other_casted->m_exps.size())
		{
			return false;
		}

		for (const auto &it : m_exps)
		{
			const auto &other_it = other_casted->m_exps.find(it.first);
			if (other_it == other_casted->m_exps.end())
			{
				return false;
			}

			if (*(other_it->second) != *(it.second))
			{
				return false;
			}
		}

		return true;
	}

	Interpreter::Value *MapExpression::evaluate(Interpreter::VirtualMachine *virtual_machine) const
	{
		Interpreter::Value *map = new Interpreter::Value();

		for (const auto &it : m_exps)
		{
			Interpreter::Value *exp_value = it.second->evaluate(virtual_machine);
			map->set_value(it.first, exp_value);
		}

		return map;
	}

	//////
	BinaryExpression::~BinaryExpression()
	{
		delete m_right;
		delete m_left;
	}

	bool BinaryExpression::operator==(const Node &other) const
	{
		const BinaryExpression *other_casted = dynamic_cast<const BinaryExpression *>(&other);
		if (!other_casted)
		{
			return false;
		}

		return m_left == other_casted->m_left && m_right == other_casted->m_right && m_op == other_casted->m_op;
	}

	Interpreter::Value *BinaryExpression::evaluate(Interpreter::VirtualMachine *virtual_machine) const
	{
		if (!m_left || !m_right)
		{
			virtual_machine->raise_exception(Interpreter::InvalidExpressionException());
			return nullptr;
		}

		Interpreter::Value *lhs = m_left->evaluate(virtual_machine);
		Interpreter::Value *rhs = m_right->evaluate(virtual_machine);

		Interpreter::Value *ret = nullptr;

		switch (m_op)
		{
		case Operator::ADD:
			ret = (*lhs + *rhs).make_copy();
			break;
		case Operator::SUB:
			ret = (*lhs - *rhs).make_copy();
			break;
		case Operator::MUL:
			ret = (*lhs * *rhs).make_copy();
			break;
		case Operator::DIV:
			ret = (*lhs / *rhs).make_copy();
			break;
		case Operator::AND:
			ret = new Interpreter::Value(lhs->is_truthy() && rhs->is_truthy());
			break;
		case Operator::OR:
			ret = new Interpreter::Value(lhs->is_truthy() || rhs->is_truthy());
			break;
		case Operator::EQUAL:
			ret = new Interpreter::Value(lhs->is_equal(*rhs));
			break;
		case Operator::NOTEQUAL:
			ret = new Interpreter::Value(!lhs->is_equal(*rhs));
			break;
		case Operator::GREATER:
			ret = new Interpreter::Value(lhs->as_number() > rhs->as_number());
			break;
		case Operator::LESSER:
			ret = new Interpreter::Value(lhs->as_number() < rhs->as_number());
			break;
		case Operator::GREATEREQUAL:
			ret = new Interpreter::Value(lhs->as_number() >= rhs->as_number());
			break;
		case Operator::LESSEREQUAL:
			ret = new Interpreter::Value(lhs->as_number() <= rhs->as_number());
			break;
		default:
			virtual_machine->raise_exception(Interpreter::InvalidExpressionException());
			break;
		}

		// Delete evaluated results
		delete lhs;
		delete rhs;

		return ret;
	}

	void BinaryExpression::print(std::size_t depth) const
	{
		std::string tabs(depth, '-');
		std::string op_str;

		switch (m_op)
		{
		case Operator::ADD:
			op_str = "+";
			break;
		case Operator::SUB:
			op_str = "-";
			break;
		case Operator::MUL:
			op_str = "*";
			break;
		case Operator::DIV:
			op_str = "/";
			break;
		case Operator::AND:
			op_str = "&&";
			break;
		case Operator::OR:
			op_str = "||";
			break;
		case Operator::EQUAL:
			op_str = "==";
			break;
		case Operator::NOTEQUAL:
			op_str = "!=";
			break;
		case Operator::GREATER:
			op_str = ">";
			break;
		case Operator::LESSER:
			op_str = "<";
			break;
		case Operator::GREATEREQUAL:
			op_str = ">=";
			break;
		case Operator::LESSEREQUAL:
			op_str = "<=";
			break;
		default:
			op_str = "(unknown)";
			break;
		}

		LOGGER.Raw(stdout, "%s> %s - %s\n", tabs.c_str(), "BinaryExpression", op_str.c_str());

		m_left->print(depth + 1);
		m_right->print(depth + 1);
	}

	//////
	LiteralExpression::~LiteralExpression()
	{
		delete m_value;
	}

	void LiteralExpression::print(std::size_t depth) const
	{
		std::string tabs(depth, '-');
		LOGGER.Raw(stdout, "%s> %s - %s\n", tabs.c_str(), "LiteralExpression", m_value->to_string().c_str());
	}

	bool LiteralExpression::operator==(const Node &other) const
	{
		const LiteralExpression *other_casted = dynamic_cast<const LiteralExpression *>(&other);
		if (!other_casted)
		{
			return false;
		}

		return m_value->is_equal(*(other_casted->m_value));
	}

	Interpreter::Value *LiteralExpression::evaluate(Interpreter::VirtualMachine *virtual_machine) const
	{
		if (!m_value)
		{
			virtual_machine->raise_exception(Interpreter::VariableNotFoundException("LITERAL"));
			return nullptr;
		}

		return m_value->make_copy();
	}

	//////
	void StoredExpression::print(std::size_t depth) const
	{
		std::string tabs(depth, '-');
		LOGGER.Raw(stdout, "%s> %s - %s\n", tabs.c_str(), "StoredExpression", m_value_identifier.c_str());
	}

	bool StoredExpression::operator==(const Node &other) const
	{
		const StoredExpression *other_casted = dynamic_cast<const StoredExpression *>(&other);
		if (!other_casted)
		{
			return false;
		}

		return m_value_identifier == other_casted->m_value_identifier;
	}

	Interpreter::Value *StoredExpression::evaluate(Interpreter::VirtualMachine *virtual_machine) const
	{
		if (m_value_identifier.find('(') != std::string::npos)
		{
			std::string identifier;
			std::string list_index;
			bool parsing_index = false;
			for (const char &c : m_value_identifier)
			{
				if (c == '\"')
				{
					continue;
				}

				if (c == ')')
				{
					parsing_index = false;
					continue;
				}

				if (c == '(')
				{
					parsing_index = true;
					continue;
				}

				if (parsing_index)
				{
					list_index += c;
					continue;
				}

				identifier += c;
			}

			if (m_value_identifier.find('\"') != std::string::npos)
			{
				Interpreter::Value *map = virtual_machine->get_value(identifier);
				if (!map)
				{
					virtual_machine->raise_exception(Interpreter::VariableNotFoundException(m_value_identifier));
					return nullptr;
				}

				const Interpreter::Value *element = map->get_value(list_index);
				if (!element)
				{
					virtual_machine->raise_exception(Interpreter::VariableNotFoundException(m_value_identifier));
					return nullptr;
				}

				return element->make_copy();
			}

			Interpreter::Value *list = virtual_machine->get_value(identifier);
			if (!list)
			{
				virtual_machine->raise_exception(Interpreter::VariableNotFoundException(m_value_identifier));
				return nullptr;
			}

			const Interpreter::Value *element = list->get_value(std::stoi(list_index));
			if (!element)
			{
				virtual_machine->raise_exception(Interpreter::VariableNotFoundException(m_value_identifier));
				return nullptr;
			}

			return element->make_copy();
		}

		Interpreter::Value *value = virtual_machine->get_value(m_value_identifier);
		if (!value)
		{
			virtual_machine->raise_exception(Interpreter::VariableNotFoundException(m_value_identifier));
			return nullptr;
		}

		return value->make_copy();
	}

	//////
	CallExpression::~CallExpression()
	{
		delete m_params;
	}

	void CallExpression::print(std::size_t depth) const
	{
		std::string tabs(depth, '-');
		LOGGER.Raw(stdout, "%s> %s - %s\n", tabs.c_str(), "CallExpression", m_func_identifier.c_str());

		m_params->print(depth + 1);
	}

	bool CallExpression::operator==(const Node &other) const
	{
		const CallExpression *other_casted = dynamic_cast<const CallExpression *>(&other);
		if (!other_casted)
		{
			return false;
		}

		if (m_func_identifier != other_casted->m_func_identifier)
		{
			return false;
		}

		const Expression &params = *m_params;
		const Expression &other_params = *(other_casted->m_params);
		return params == other_params;
	}

	Interpreter::Value *CallExpression::evaluate(Interpreter::VirtualMachine *virtual_machine) const
	{
		Interpreter::Value *params = nullptr;
		if (m_params)
		{
			params = m_params->evaluate(virtual_machine);
		}

		if (m_func_identifier.find("native_") != std::string::npos)
		{
			std::string func_name = m_func_identifier.substr(7);

			if (!virtual_machine->call_native(func_name, params))
			{
				virtual_machine->raise_exception(Interpreter::FunctionNotFoundException(func_name));
				delete params;
				return new Interpreter::Value(false);
			}

			delete params;
			return new Interpreter::Value(true);
		}

		Interpreter::Func *func = virtual_machine->get_function(m_func_identifier);
		if (!func)
		{
			virtual_machine->raise_exception(Interpreter::FunctionNotFoundException(m_func_identifier));
			return nullptr;
		}

		return func->execute(virtual_machine, m_params)->make_copy();
	}

};
