// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "node.h"

#include <cstdint>
#include <unordered_map>
#include <string>
#include <utility>
#include <vector>

namespace Interpreter
{
	class Value;

	namespace VM
	{
		class ASTWalker;
	};

	typedef VM::ASTWalker VirtualMachine;

};

// NOTE: Expressions always evaluate to newly created Values

namespace AST
{
	class Expression : public Node
	{
	public:
		~Expression() override = default;

		// NOTE: The caller owns the value
		virtual Interpreter::Value *evaluate(Interpreter::VirtualMachine *virtual_machine) const = 0;

	protected:
		Expression() = default;
	};

	class ListExpression final : public Expression
	{
	public:
		ListExpression(std::vector<Expression *> expressions) : m_exps(std::move(expressions)){};

		~ListExpression() override;

		bool operator==(const Node &) const override;

		Interpreter::Value *evaluate(Interpreter::VirtualMachine *virtual_machine) const override;

		void print(std::size_t depth) const override;

	private:
		std::vector<Expression *> m_exps;
	};

	class MapExpression final : public Expression
	{
	public:
		MapExpression(std::unordered_map<std::string, Expression *> expressions) : m_exps(std::move(expressions)){};

		~MapExpression() override;

		bool operator==(const Node &) const override;

		Interpreter::Value *evaluate(Interpreter::VirtualMachine *virtual_machine) const override;

		void print(std::size_t depth) const override;

	private:
		std::unordered_map<std::string, Expression *> m_exps;
	};

	class BinaryExpression final : public Expression
	{
	public:
		enum Operator
		{
			ADD,
			SUB,
			MUL,
			DIV,
			AND,
			OR,
			EQUAL,
			NOTEQUAL,
			GREATER,
			LESSER,
			LESSEREQUAL,
			GREATEREQUAL,
		};

		BinaryExpression(Expression *left, Expression *right, Operator op) : m_left(left), m_right(right), m_op(op){};

		~BinaryExpression() override;

		bool operator==(const Node &) const override;

		Interpreter::Value *evaluate(Interpreter::VirtualMachine *virtual_machine) const override;

		void print(std::size_t depth) const override;

	private:
		Expression *m_left;
		Expression *m_right;
		Operator m_op;
	};

	class LiteralExpression final : public Expression
	{
	public:
		explicit LiteralExpression(Interpreter::Value *value) : m_value(value){};

		~LiteralExpression() override;

		bool operator==(const Node &) const override;

		Interpreter::Value *evaluate(Interpreter::VirtualMachine *virtual_machine) const override;

		void print(std::size_t depth) const override;

	private:
		Interpreter::Value *m_value;
	};

	class StoredExpression final : public Expression
	{
	public:
		explicit StoredExpression(std::string value_identifier) : m_value_identifier(std::move(value_identifier)){};

		~StoredExpression() override = default;

		bool operator==(const Node &) const override;

		Interpreter::Value *evaluate(Interpreter::VirtualMachine *virtual_machine) const override;

		void print(std::size_t depth) const override;

	private:
		std::string m_value_identifier;
	};

	class CallExpression final : public Expression
	{
	public:
		explicit CallExpression(std::string func_identifier, MapExpression *params) : m_func_identifier(std::move(func_identifier)), m_params(params){};

		~CallExpression() override;

		bool operator==(const Node &) const override;

		Interpreter::Value *evaluate(Interpreter::VirtualMachine *virtual_machine) const override;

		void print(std::size_t depth) const override;

	private:
		std::string m_func_identifier;
		MapExpression *m_params;
	};

}
