// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "node.h"

#include <cstdint>
#include <vector>

namespace REPL
{

	class REPL;

};

namespace Interpreter
{
	class Value;

	namespace VM
	{
		class ASTWalker;
	};

	typedef VM::ASTWalker VirtualMachine;

};

namespace AST
{
	class Statement;
	class MapExpression;

	class Scope final : public Node
	{
	public:
		Scope() = default;

		~Scope() override;

		bool operator==(const Node &) const override;

		void print(std::size_t depth) const override;

		void execute(Interpreter::VirtualMachine *virtual_machine, MapExpression *params) const;

		void append(Statement *statement);

	private:
		std::vector<Statement *> m_children;

		friend class REPL::REPL;
	};
};
