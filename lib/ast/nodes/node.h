// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <cstdint>

namespace AST
{
	class Node
	{
	public:
		virtual ~Node() = default; // Only call derived destructor
		virtual void print(std::size_t depth) const = 0;

		virtual bool operator==(const Node &) const = 0;

	protected:
		Node() = default; // Not directly constructible

		// NONCOPYABLE
		Node(const Node &) = delete;

		Node(const Node &&) = delete;

		Node &operator=(const Node &) = delete;

		Node &operator=(Node &&) = delete;
	};
};
