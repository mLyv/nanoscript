// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "statements.h"

#include "expressions.h"
#include "scope.h"
#include "interpreter/data_handles/func.h"
#include "interpreter/data_handles/value.h"
#include "interpreter/vm/ast_walker/ast_walker.h"
#include "interpreter/vm/exception/exception.h"
#include "logger/logger.h"

namespace Interpreter
{
	typedef VM::ASTWalker VirtualMachine;
};

namespace AST
{

	///////
	ReturnStatement::~ReturnStatement()
	{
		delete m_exp;
	}

	void ReturnStatement::execute(Interpreter::VirtualMachine *virtual_machine) const
	{
		if (!m_exp)
		{
			virtual_machine->raise_exception(Interpreter::InvalidExpressionException());
			return;
		}

		Interpreter::Value *result = m_exp->evaluate(virtual_machine);

		virtual_machine->set_value(Interpreter::VirtualMachine::SCOPE_EXIT_VALUE, result);
	}

	bool ReturnStatement::operator==(const Node &other) const
	{
		const ReturnStatement *other_casted = dynamic_cast<const ReturnStatement *>(&other);
		if (!other_casted)
		{
			return false;
		}

		return *(m_exp) == *(other_casted->m_exp);
	}

	void ReturnStatement::print(std::size_t depth) const
	{
		std::string tabs(depth, '-');
		LOGGER.Raw(stdout, "%s> %s\n", tabs.c_str(), "ReturnStatement");

		m_exp->print(depth + 1);
	}

	///////
	AssignStatement::~AssignStatement()
	{
		delete m_exp;
	}

	void AssignStatement::execute(Interpreter::VirtualMachine *virtual_machine) const
	{
		if (!m_exp)
		{
			virtual_machine->raise_exception(Interpreter::InvalidExpressionException());
			return;
		}

		Interpreter::Value *result = m_exp->evaluate(virtual_machine);

		if (!result)
		{
			virtual_machine->raise_exception(Interpreter::InvalidExpressionException());
			delete result;
			return;
		}

		if (m_value_identifier.find('(') != std::string::npos)
		{
			std::string identifier;
			std::string list_index;
			bool parsing_index = false;
			for (const char &c : m_value_identifier)
			{
				if (c == '\"')
				{
					continue;
				}

				if (c == ')')
				{
					parsing_index = false;
					continue;
				}

				if (c == '(')
				{
					parsing_index = true;
					continue;
				}

				if (parsing_index)
				{
					list_index += c;
					continue;
				}

				identifier += c;
			}

			Interpreter::Value *value = virtual_machine->get_value(identifier);

			if (!value)
			{
				virtual_machine->raise_exception(Interpreter::VariableNotFoundException(identifier));
				delete result;
				return;
			}

			if (m_value_identifier.find('\"') != std::string::npos)
			{
				value->set_value(list_index, result);
				return;
			}

			value->set_value(std::stoi(list_index), result);
			return;
		}

		virtual_machine->set_value(m_value_identifier, result);
	}

	bool AssignStatement::operator==(const Node &other) const
	{
		const AssignStatement *other_casted = dynamic_cast<const AssignStatement *>(&other);
		if (!other_casted)
		{
			return false;
		}

		if (m_value_identifier != other_casted->m_value_identifier)
		{
			return false;
		}

		return *(m_exp) == *(other_casted->m_exp);
	}

	void AssignStatement::print(std::size_t depth) const
	{
		std::string tabs(depth, '-');
		LOGGER.Raw(stdout, "%s> %s - %s\n", tabs.c_str(), "AssignStatement", m_value_identifier.c_str());

		m_exp->print(depth + 1);
	}

	///////
	void FunctionDeclarationStatement::execute(Interpreter::VirtualMachine *virtual_machine) const
	{
		if (!m_body)
		{
			virtual_machine->raise_exception(Interpreter::InvalidScopeException());
			return;
		}

		Interpreter::Func *func = new Interpreter::Func(m_body);
		virtual_machine->set_function(m_name, func);
	}

	bool FunctionDeclarationStatement::operator==(const Node &other) const
	{
		const FunctionDeclarationStatement *other_casted = dynamic_cast<const FunctionDeclarationStatement *>(&other);
		if (!other_casted)
		{
			return false;
		}

		if (m_name != other_casted->m_name)
		{
			return false;
		}

		const Node &body = *m_body;
		const Node &other_body = *(other_casted->m_body);
		return body == other_body;
	}

	void FunctionDeclarationStatement::print(std::size_t depth) const
	{
		std::string tabs(depth, '-');
		LOGGER.Raw(stdout, "%s> %s - %s\n", tabs.c_str(), "FunctionDeclarationStatement", m_name.c_str());

		m_body->print(depth + 1);
	}

	///////
	void VoidStatement::print(std::size_t depth) const
	{
		std::string tabs(depth, '-');
		LOGGER.Raw(stdout, "%s> %s\n", tabs.c_str(), "VoidStatement");
	}

	bool VoidStatement::operator==(const Node &other) const
	{
		const VoidStatement *other_casted = dynamic_cast<const VoidStatement *>(&other);
		if (!other_casted)
		{
			return false;
		}

		return true;
	}

	///////
	AggregateStatement::~AggregateStatement()
	{
		for (const auto statement : m_statements)
		{
			delete statement;
		}
	}

	void AggregateStatement::append(Statement *statement)
	{
		m_statements.push_back(statement);
	}

	bool AggregateStatement::operator==(const Node &other) const
	{
		const AggregateStatement *other_casted = dynamic_cast<const AggregateStatement *>(&other);
		if (!other_casted)
		{
			return false;
		}

		if (m_statements.size() != other_casted->m_statements.size())
		{
			return false;
		}

		for (size_t i = 0; i < m_statements.size(); ++i)
		{
			if (*(m_statements[i]) != *(other_casted->m_statements[i]))
			{
				return false;
			}
		}

		return true;
	}

	void AggregateStatement::execute(Interpreter::VirtualMachine *virtual_machine) const
	{
		// No stack pushing here
		for (const auto statement : m_statements)
		{
			statement->execute(virtual_machine);
		}
	}

	void AggregateStatement::print(std::size_t depth) const
	{
		std::string tabs(depth, '-');
		LOGGER.Raw(stdout, "%s> %s\n", tabs.c_str(), "AggregateStatement");

		for (const auto statement : m_statements)
		{
			statement->print(depth + 1);
		}
	}

	///////
	ConditionalStatement::~ConditionalStatement()
	{
		delete m_false_statement;
		delete m_true_statement;
		delete m_condition;
	}

	bool ConditionalStatement::operator==(const Node &other) const
	{
		const ConditionalStatement *other_casted = dynamic_cast<const ConditionalStatement *>(&other);
		if (!other_casted)
		{
			return false;
		}

		if (*m_condition != *(other_casted->m_condition))
		{
			return false;
		}

		return m_true_statement == other_casted->m_true_statement && m_false_statement == other_casted->m_false_statement;
	}

	void ConditionalStatement::execute(Interpreter::VirtualMachine *virtual_machine) const
	{
		if (!m_condition)
		{
			virtual_machine->raise_exception(Interpreter::InvalidExpressionException());
			return;
		}

		Interpreter::Value *result = m_condition->evaluate(virtual_machine);

		if (result->as_boolean())
		{
			m_true_statement->execute(virtual_machine);
		}
		else
		{
			m_false_statement->execute(virtual_machine);
		}

		delete result;
	}

	void ConditionalStatement::print(std::size_t depth) const
	{
		std::string tabs(depth, '-');
		LOGGER.Raw(stdout, "%s> %s\n", tabs.c_str(), "ConditionalStatement");

		m_condition->print(depth + 1);
		m_true_statement->print(depth + 1);
		m_false_statement->print(depth + 1);
	}

	///////
	LoopStatement::~LoopStatement()
	{
		delete m_statement;
		delete m_condition;
	}

	bool LoopStatement::operator==(const Node &other) const
	{
		const LoopStatement *other_casted = dynamic_cast<const LoopStatement *>(&other);
		if (!other_casted)
		{
			return false;
		}

		if (*m_condition != *(other_casted->m_condition))
		{
			return false;
		}

		return m_statement == other_casted->m_statement;
	}

	void LoopStatement::execute(Interpreter::VirtualMachine *virtual_machine) const
	{
		while (true)
		{
			if (!m_condition)
			{
				virtual_machine->raise_exception(Interpreter::InvalidExpressionException());
				return;
			}

			Interpreter::Value *result = m_condition->evaluate(virtual_machine);
			if (result->as_boolean())
			{
				m_statement->execute(virtual_machine);
				delete result;
			}
			else
			{
				delete result;
				break;
			}
		}
	}

	void LoopStatement::print(std::size_t depth) const
	{
		std::string tabs(depth, '-');
		LOGGER.Raw(stdout, "%s> %s\n", tabs.c_str(), "LoopStatement");

		m_condition->print(depth + 1);
		m_statement->print(depth + 1);
	}

};
