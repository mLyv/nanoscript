// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "scope.h"

#include "expressions.h"
#include "statements.h"
#include "interpreter/data_handles/value.h"
#include "interpreter/vm/ast_walker/ast_walker.h"
#include "logger/logger.h"

namespace Interpreter
{
	typedef VM::ASTWalker VirtualMachine;
};

namespace AST
{

	///////
	Scope::~Scope()
	{
		for (const auto child : m_children)
		{
			delete child;
		}
	}

	void Scope::append(Statement *statement)
	{
		m_children.push_back(statement);
	}

	void Scope::print(std::size_t depth) const
	{
		std::string tabs(depth, '-');
		LOGGER.Raw(stdout, "%s> %s\n", tabs.c_str(), "Scope");

		for (const auto child : m_children)
		{
			child->print(depth + 1);
		}
	}

	void Scope::execute(Interpreter::VirtualMachine *virtual_machine, MapExpression *params) const
	{
		Interpreter::Value *args = nullptr;
		if (params)
		{
			// not leaked because VM cleans it up
			args = params->evaluate(virtual_machine);
		}

		virtual_machine->push_stack();

		if (args)
		{
			virtual_machine->set_value("args", args);
		}

		for (const auto child : m_children)
		{
			child->execute(virtual_machine);

			if (virtual_machine->has_error())
			{
				break;
			}
		}

		virtual_machine->pop_stack();
	}

	bool Scope::operator==(const Node &other) const
	{
		const Scope *other_casted = dynamic_cast<const Scope *>(&other);
		if (!other_casted)
		{
			return false;
		}

		if (m_children.size() != other_casted->m_children.size())
		{
			return false;
		}

		for (size_t i = 0; i < m_children.size(); ++i)
		{
			if (*(m_children[i]) != *(other_casted->m_children[i]))
			{
				return false;
			}
		}

		return true;
	}

};
