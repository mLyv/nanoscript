// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "filereader.h"

#include "logger/logger.h"

#include <filesystem>
#include <fstream>

namespace FileSystem
{

	std::string read_file(const std::string &filename)
	{
		std::string ret;

		std::filesystem::path path = filename;
		std::filesystem::path cwd = std::filesystem::current_path();

		std::filesystem::path absolute_path =
			cwd / path; // std::filesystem::absolute seems dubious on non-posix systems
		std::filesystem::path canonical_path = std::filesystem::canonical(absolute_path);

		LOGGER.Info("Attempting to read {%s}", canonical_path.c_str());

		std::ifstream filestream(absolute_path);

		if (!filestream)
		{
			LOGGER.Error("%s cannot be read", canonical_path.c_str());
			return ret;
		}

		std::stringstream contents;
		contents << filestream.rdbuf();

		return contents.str();
	}

};
