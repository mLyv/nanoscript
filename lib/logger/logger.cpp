// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "logger.h"

namespace Logger
{

	Logger &Logger::Instance()
	{
		static Logger instance;
		return instance;
	}

	void Logger::SetPrintFunction(PrintFunction print_function)
	{
		m_fprintf = print_function;
	}

	void Logger::Raw(FILE *stream, const char *str, ...) const
	{
		va_list args;
		va_start(args, str);
		m_fprintf(stream, str, args);
		va_end(args);
	}

	void Logger::Info(const char *str, ...) const
	{
		Raw(stdout, "\nINFO::\t");

		va_list args;
		va_start(args, str);
		m_fprintf(stdout, str, args);
		va_end(args);
	}

	void Logger::Error(const char *str, ...) const
	{
		Raw(stderr, "\nERROR::\t");

		va_list args;
		va_start(args, str);
		m_fprintf(stderr, str, args);
		va_end(args);
	}

	void Logger::Warn(const char *str, ...) const
	{
		Raw(stdout, "\nWARN::\t");

		va_list args;
		va_start(args, str);
		m_fprintf(stdout, str, args);
		va_end(args);
	}
};
