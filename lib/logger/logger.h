// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <cstdarg>
#include <cstdio>

namespace Logger
{

	using PrintFunction = int (*)(FILE *, const char *, va_list);

	class Logger
	{
	public:
		// NONCOPYABLE
		Logger(const Logger &) = delete;

		Logger(const Logger &&) = delete;

		Logger &operator=(const Logger &) = delete;

		Logger &operator=(Logger &&) = delete;

		~Logger() = default;

		static Logger &Instance();

		void SetPrintFunction(PrintFunction print_function);

		void Raw(FILE *stream, const char *str, ...) const;

		void Info(const char *str, ...) const;

		void Error(const char *str, ...) const;

		void Warn(const char *str, ...) const;

	private:
		Logger() : m_fprintf(vfprintf){};

		PrintFunction m_fprintf;
	};

};

#define LOGGER Logger::Logger::Instance()
