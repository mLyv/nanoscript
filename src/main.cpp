// Copyright (C) 2022  Mohamed Lywaan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "ast/nodes/expressions.h"
#include "ast/nodes/scope.h"
#include "ast/nodes/statements.h"
#include "interpreter/data_handles/func.h"
#include "interpreter/data_handles/value.h"
#include "interpreter/vm/ast_walker/ast_walker.h"
#include "filesystem/filereader.h"
#include "lexer/lexer.h"
#include "logger/logger.h"
#include "parser/parser.h"
#include "repl/repl.h"

int main(int argc, char **argv)
{
	LOGGER.Raw(stdout, "Copyright (C) 2022  Mohamed Lywaan\nThis program comes with ABSOLUTELY NO WARRANTY.\nThis is free software, and you are welcome to redistribute it under certain conditions.\n");

	if (argc < 2)
	{
		auto repl = REPL::REPL();
		repl.main_loop();
		return 0;
	}

	std::string source_file(argv[1]);
	std::string read_source = FileSystem::read_file(source_file);

	if (read_source.empty())
	{
		LOGGER.Error("Source file is empty");
		return 1;
	}

	LOGGER.Info("Program source code: \n%s", read_source.c_str());

	auto lexer = new Lexer::Lexer(read_source);
	auto tokens = lexer->parse();

	LOGGER.Info("Lexer token list:\n");
	for (const auto &token : tokens)
	{
		LOGGER.Raw(stdout, "%s\n", token.to_string().c_str());
	}

	auto parser = new Parser::Parser(tokens);

	auto program = parser->parse();

	delete parser;
	delete lexer;

	LOGGER.Info("Program AST:\n");
	program->print(0);

	auto vm = new Interpreter::VM::ASTWalker();
	program->execute(vm, nullptr);

	// NOTE: The last stack is never popped, so read the exit value of the just "popped" stack.
	auto last_scope_exit_value = vm->get_value(Interpreter::VM::ASTWalker::SCOPE_EXIT_VALUE);
	LOGGER.Raw(stdout, "\n");
	LOGGER.Info("Exited with {%s}\n", last_scope_exit_value->to_string().c_str());

	int exit_code = last_scope_exit_value->as_number();

	delete program; // This will clean everything else
	delete vm;

	return exit_code;
}
